// Calculates the number of open orders you have with a magic number.

// Construct `OpenOrders`:
//    OpenOrders orders;
// Then use the methods to query it:
//    orders.buys();

#property copyright "Jarryd Beck"
#property strict

class OpenOrders {
  public:
  OpenOrders(int magic) 
  : m_buys(0)
  , m_buylimit(0)
  , m_buystop(0)
  , m_sells(0)
  , m_sell_limit(0)
  , m_sell_stop(0)
  {
    for (int i = 0; i != OrdersTotal(); ++i) {
      if (OrderSelect(i, SELECT_BY_POS) == false) return;
      if (OrderSymbol() != Symbol() || OrderMagicNumber() != magic) continue;
      
      switch (OrderType()) {
        case OP_BUY:
          ++m_buys;
          break;
        case OP_BUYLIMIT:
          ++m_buylimit;
          break;
        case OP_BUYSTOP:
          ++m_buystop;
          break;
        
        case OP_SELL:
          ++m_sells;
          break;
        case OP_SELLLIMIT:
          ++m_sell_limit;
          break;
        case OP_SELLSTOP:
          ++m_sell_stop;
          break;
      }
    }
  }
  
  int buys() const {
    return m_buys;
  }
  
  int buy_limit() const {
    return m_buylimit;
  }
  
  int buy_stop() const {
    return m_buystop;
  }
  
  int sells() const {
    return m_sells;
  }
  
  int sell_limit() const {
    return m_sell_limit;
  }
  
  int sell_stop() const {
    return m_sell_stop;
  }
  
  private:
  int m_buys, m_buylimit, m_buystop, m_sells, m_sell_limit, m_sell_stop;
};
