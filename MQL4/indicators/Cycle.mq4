//+------------------------------------------------------------------+
//|                                                      LowPass.mq4 |
//|                                                      Jarryd Beck |
//|                                                              foo |
//+------------------------------------------------------------------+
#property copyright "Jarryd Beck"
#property link      "foo"
#property version   "1.00"
#property strict
#property indicator_separate_window
#property indicator_buffers 4
#property indicator_plots 1

double Median[];
double Smooth[];
double Cycle[];
double CycleTrigger[];

input double Alpha = 0.07;

int OnInit()
{
   SetIndexBuffer(2, Median);
   SetIndexBuffer(3, Smooth);
   SetIndexBuffer(0, Cycle);
   SetIndexBuffer(1, CycleTrigger);
   
   SetIndexStyle(2, DRAW_NONE);
   SetIndexLabel(2, NULL);
   SetIndexStyle(3, DRAW_NONE);
   SetIndexLabel(3, NULL);
   
   SetIndexLabel(1, "Trigger");
   
   SetIndexStyle(0, DRAW_LINE, EMPTY, EMPTY, clrRed);
   SetIndexStyle(1, DRAW_LINE, EMPTY, EMPTY, clrBlue);
   
   return(INIT_SUCCEEDED);
}

// Smooth = (Price + 2*Price[1] + 2*Price[2] + Price[3])/6;
// Cycle = (1 - .5*alpha)*(1 - .5*alpha)*(Smooth - 2*Smooth[1] + Smooth[2])
//         + 2*(1 - alpha)*Cycle[1] - (1 - alpha)*(1 - alpha)*Cycle[2];

void start() {
  int counted = IndicatorCounted();
  int i = Bars - counted - 1;
  
  
  
  while (i >= 0) {
    int available = Bars - i - 1;
    Median[i] = (High[i] + Low[i])/2;
    
    if (available >= 3) {
      Smooth[i] = (Median[i] + 2*Median[i+1] + 2*Median[i+2] + Median[i+3])/6;
    } else {
      Smooth[i] = Median[i];
    }
    
    if (available < 3) {
      Cycle[i] = 0;
    } else {
      Cycle[i] = (1-0.5*Alpha)*(1-0.5*Alpha)*(Smooth[i] - 2*Smooth[i+1] + Smooth[i+2])
                 + 2*(1-Alpha)*Cycle[i+1] - (1-Alpha)*(1-Alpha)*Cycle[i+2];
    }
    
    if (available >= 1) {
      CycleTrigger[i] = Cycle[i+1];
    } else {
      CycleTrigger[i] = 0;
    }
    
    --i;
  }
}